const { src, dest, parallel, watch, series } = require('gulp')
const scss = require ('gulp-sass')
const gulp = require('gulp')
const browserSync = require('browser-sync').create()
reload = browserSync.reload
var postcss = require('gulp-postcss');

gulp.task('browser-sync', function() {
    browserSync.init({
        server: "./"
    });

    browserSync.watch('./**/*.*').on('change', browserSync.reload);
});

gulp.task('autoprefixer', function () {
    var postcss      = require('gulp-postcss');
    var sourcemaps   = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');
 
    return gulp.src('./css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./dest'));
});

const path = {
    scss: 'scss/**/*.scss',
    css: 'css',
    html: '*.html'
}

function css () {
    return src('scss/main.scss')
        .pipe(scss().on('error', scss.logError))
        .pipe(dest('css'))
}

watch(path.scss, css)

exports.css = css
exports.default = parallel(css)
