$('.js-menu-mobile-btn').click(() => {
    $('.js-menu-node').toggleClass('-open')
})


const SLIDER_MAIN_SPEED = 300
const SLIDER_MAIN_DELAY = 6000

const progressBarStartAnimation = () => {
    if ($('.js-slider-main-progress-bar-container').hasClass('-pause')) {
        return
    }

    const percentageComplete = 1
    const progressBar = $('.js-slider-main-progress-bar')
    const strokeDashOffsetValue = 100 - (percentageComplete * 100)
    progressBar.css('stroke-dashoffset', 100)
    progressBar.stop()
    progressBar.animate({
        'stroke-dashoffset': strokeDashOffsetValue
    }, {
        step: function (value) {
            $(this).css('stroke-dashoffset', value);
        },
        duration: SLIDER_MAIN_DELAY + SLIDER_MAIN_SPEED
    })
}

const progressBarStopAnimation = () => {
    const progressBar = $('.js-slider-main-progress-bar')
    progressBar.css('stroke-dashoffset', 0)
    progressBar.stop()
    $('.js-slider-main-progress-bar-container').addClass('-pause')
}

const sliderBlyat = () => {

    var slide = $('.swiper-slide-active').data('swiperSlideIndex');

    $('.welcome_bg').removeClass('_visible');
    $($('.welcome_bg')[slide]).addClass('_visible'); 
}

const sliderMain = new Swiper ('.js-slider-main', {
    // effect: 'fade',
    autoHeight: false,
    loop: true,
    speed: SLIDER_MAIN_SPEED,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    autoplay: {
        delay: SLIDER_MAIN_DELAY
    },
    on: {
        slideChange: progressBarStartAnimation,
        // autoplayStart: progressBarStartAnimation,
        autoplayStop: progressBarStopAnimation,
        slideChangeTransitionStart: sliderBlyat,
        init: sliderBlyat
    },
    fadeEffect: {
        crossFade: true
    }
})

$('.js-slider-main-progress-bar-container').click(function () {
    if ($(this).hasClass('-pause')) {
        $(this).removeClass('-pause')
        progressBarStartAnimation()
        sliderMain.autoplay.start()
    } else {
        progressBarStopAnimation()
        sliderMain.autoplay.stop()
    }
})


const sliderKnow = new Swiper ('.js-slider-know', {
    effect: 'fade',
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    }
})

$('.js-course-filters').click(function () {
    $('.courses_filtersItems').toggleClass('-open')
})

const sliderInst = new Swiper ('.js-slider-inst', {
    loop: true
})

$('.js-slider-inst-next').click(() => {
    sliderInst.slideNext()
})

$('.js-slider-inst-prev').click(() => {
    sliderInst.slidePrev()
})

$('.js-feedback-modal-open').click(function () {
    $('.modal').addClass('-open')
})

$('.js-feedback-modal-close').click(function () {
    $('.modal').removeClass('-open')
})

$('.courses_tab').click(function (e) {
    e.preventDefault();
    var currentId = $(this).data('id');

    $('.courses_tab').removeClass('-active');
    $(this).addClass('-active');
    $('.courses_filter').removeClass('-active');

    $('.courses_filter').each(function () {
        var currentFilter = $(this);

        if (currentId == $(this).data('id')) {
            currentFilter.addClass('-active');
        }
    })

    $('.course').each(function () {
        $(this).addClass('-show');
        $(this).removeClass('-hide');

        if ($('.courses_tab.-active').hasClass('-all')) {
            $('.course').addClass('-show');
            $('.course').removeClass('-hide');
        } else if ($(this).data('id') != currentId) {
            $(this).addClass('-hide');
        }
    })
})

$('.courses_filter').click(function (e) {
    e.preventDefault();
    var currentId = $(this).data('id');

    $('.courses_filter').removeClass('-active');
    $(this).addClass('-active');
    $('.courses_tab').removeClass('-active');

    $('.courses_tab').each(function () {
        var currentFilter = $(this);

        if (currentId == $(this).data('id')) {
            currentFilter.addClass('-active');
        }
    })

    $('.course').each(function () {
        $(this).addClass('-show');
        $(this).removeClass('-hide');

        if ($('.courses_filter.-active').hasClass('-all')) {
            $('.course').addClass('-show');
            $('.course').removeClass('-hide');
        } else if ($(this).data('id') != currentId) {
            $(this).addClass('-hide');
        }
    })
})